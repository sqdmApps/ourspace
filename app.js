const { app, BrowserWindow, ipcMain } = require("electron");
let path = require('path');
const Store = require("electron-store");
let appWin;
const store = new Store();
createWindow = () => {
    appWin = new BrowserWindow({
        width: 1232,
        height: 768,
        minWidth: 400,
        minHeight: 768,
        webPreferences: {
            contextIsolation: false,
            nodeIntegration: true
        },
    });
    appWin.loadURL(`file://${__dirname}/build/index.html`);
    appWin.setMenu(null);
    appWin.webContents.openDevTools();
    appWin.on("closed", () => {
        appWin = null;
    });
};
app.on("ready", createWindow);
app.on("window-all-closed", () => {
    if (process.platform !== "darwin") {
        app.quit();
    }
});
