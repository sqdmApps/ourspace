# OurSpace

[![N|Solid](./src/assets/images/opengraph/0000p.png)](https://ourspace.sqdm.com/)

Este proyecto contendrá todo lo necesario para el desarrollo de la intranet de [SQDM](https://sqdm.com) la cual estará desarrollada en Angular con conexiones a varios servicios de la cual obtener la información y realizar las operaciones de CRUD necesarias _/ this project will contain everything necessary for the development of the intranet of [SQDM](https://sqdm.com) which will be developed in Angular with connections to various services from which to obtain information and perform the necessary CRUD operations_.

**Índice / _index_**

[TOC]

## 1 - Lanzar en modo de desarrollo _/ Launch in development mode_

Ejecute el comando `npm i --force` tras finalizar este proceso ejecute el comando `npm run server` para lanzar un servidor de desarrollo al que podrás acceder desde la url: `http://localhost:4200/`, la aplicación se recargará automáticamente si cambias alguno de los archivos _/ run the command `npm i --force` after finishing this process run the command `npm run server` to launch a development server which you can access from the url: `http://localhost:4200/`, the application will automatically reload if you change any of the files_.

## 2 - Lanzar una preview de una aplicación de escritorio _/ launch a preview of a desktop application_

El Qix permite crear app de escritorio para Windows o MacOS y estas se pueden visualizar en una version tipo demo ejecutando el comando `npm run app-desktop` en una terminal _/ Qix allows you to create desktop apps for Windows or MacOS and these can be viewed in a demo version by executing the command `npm run app-desktop` in a terminal_.
