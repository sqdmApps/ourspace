// This file can be replaced during build by using the `fileReplacements` array.
// `ng build` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,

  api_documents: "https://graph.microsoft.com/v1.0/sites/root/sites/sqdmlabs.sharepoint.com,70dbdf8e-0301-41be-a510-bfae21557bd8,35e48428-c261-4444-aaa7-a1dc962f512f/drive/root:/",
  api_microsoft_graph: "https://graph.microsoft.com/v1.0/",
  api_database: "https://graph.microsoft.com/v1.0/sites/root/sites/sqdmlabs.sharepoint.com,70dbdf8e-0301-41be-a510-bfae21557bd8,35e48428-c261-4444-aaa7-a1dc962f512f/lists/",
  redirect_url: "http://localhost:4200/",

  api_key_communications: "97d9d4eb-1433-472c-b308-f42111697611",
  api_key_email_from: "aplicacionessqdm@gmail.com",
  api_key_email_host: "smtp.elasticemail.com",
  api_key_email_password: "C242EF2530F1E4649ACC750C7FFB735D4E0D",
  api_key_email_to: "jhaircausil@sqdm.com",
  api_key_email_user_name: "aplicacionessqdm@gmail.com",
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/plugins/zone-error';  // Included with Angular CLI.
