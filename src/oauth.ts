import { environment } from "./environments/environment";

export const o_auth_settings = {
  appId: "f8f2eed1-950d-44bd-ac68-3e33afbf4892",
  response_type: "code",
  redirectUri: environment.redirect_url,
  scopes: [
    "user.read",
    "user.readwrite",
    "mailboxsettings.read",
    "mail.send",
    "calendars.readwrite",
    "files.readwrite",
    "sites.readwrite.all",
  ],
};