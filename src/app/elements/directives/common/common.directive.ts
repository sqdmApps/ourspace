import { Directive, ElementRef, EventEmitter, HostListener, Input, Output } from '@angular/core';
import { CommonService } from '../../services/common/common.service';
import { TranslateService } from '@ngx-translate/core';

@Directive({
  selector: "[qix]",
})
export class CommonDirective {
  @Input("tooltip") tooltip!: string;

  constructor(
    private element_ref: ElementRef,
    private s_common: CommonService,
    private s_translation: TranslateService,
  ) { }

  ngOnInit() {
    if (this.tooltip) {
      let tooltip = this.s_translation.instant(this.tooltip);
      this.element_ref.nativeElement.title = tooltip;
    };
  }

  public animate_qix = (element: any, animation: any, prefix = 'animate__') =>
    new Promise((resolve, reject) => {
      const animation_name = `${prefix}${animation}`;
      const to_animate = element;
      to_animate.classList.add(`${prefix}animated`, animation_name);

      function handleAnimationEnd(event: any) {
        event.stopPropagation();
        to_animate.classList.remove(`${prefix}animated`, animation_name);
        resolve('Animation ended');
      };
      to_animate.addEventListener('animationend', handleAnimationEnd, {
        once: true
      });
    });
}
