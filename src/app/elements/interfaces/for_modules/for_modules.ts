import { i_file, i_date, i_links } from "../common/common";

export interface i_communication {
    creation_date: i_date;
    external_links?: i_links[];
    id: any;
    mail_piece: i_file;
    modification_date?: i_date;
    title: string;
};

export interface i_modules {
    children?: i_process[];
    creation_date?: i_date;
    description?: string;
    id?: any;
    letter?: string;
    modification_date?: i_date;
    name: string;
    path?: string;
    target?: string;
};

export interface i_multimedia {
    creation_date?: i_date;
    id?: any;
    modification_date?: i_date;
    multimedia: i_file;
    name?: string;
    selected?: boolean;
    show_hide?: boolean;
};

export interface i_pager {
    back?: string;
    next?: string;
};

export interface i_post {
    alignment: string;
    background_image: i_file;
    background_mask: string;
    creation_date?: i_date;
    id?: any;
    modification_date?: i_date;
    template: string;
    text: string;
    title: string;
};

export interface i_process {
    creation_date?: i_date;
    description: string;
    id?: any;
    letter: string;
    modification_date?: i_date;
    name: string;
    parent_id?: string;
    path?: string;
    target?: string;
};

export interface i_qudo {
    creation_date: i_date;
    id: any;
    message: string;
    modification_date: i_date;
    sticker: i_multimedia;
    title: string;
};

export interface i_space {
    creation_date?: i_date;
    code_svg: string;
    description: string;
    id?: any;
    modification_date?: i_date;
    name: string;
};

export interface i_titulation {
    creation_date?: i_date;
    description: string;
    id?: any;
    modification_date?: i_date;
    show_hide: boolean;
    title: string;
};

export interface i_request_body_mail {
    contentType: string;
    content: string;
};

export interface i_request_email_address_mail {
    address: string;
};

export interface i_request_message_mail {
    subject: string;
    body: i_request_body_mail;
    toRecipients: i_request_to_recipients_mail[];
};

export interface i_request_send_mail {
    message: i_request_message_mail
}

export interface i_request_to_recipients_mail {
    emailAddress: i_request_email_address_mail;
};