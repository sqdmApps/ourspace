import { SafeUrl } from "@angular/platform-browser";

export interface i_colors {
    hex: any;
    hsl: i_hsl;
    rgb: i_rgb;
};

export interface i_columns {
    all: any[];
    columns: i_columns_pagination;
    count: number;
};

export interface i_columns_pagination {
    left: any[],
    right: any[];
};

export interface i_country {
    code: string;
    currencies: i_currencies,
    flag: any,
    language: i_language,
    maps: i_maps,
    name: string;
};

export interface i_currencies {
    abbreviation: string;
    name: string;
    symbol: string;
};

export interface i_data_send {
    data?: any;
    from: string;
};

export interface i_date {
    placeholder: string;
    value: Date | any;
};

export interface i_devices {
    size: i_size_devices;
    device: i_information_devices;
};

export interface i_events_action {
    element: any;
    event: string;
    value?: any;
};

export interface i_feedback {
    message: string;
    type: string;
};

export interface i_file {
    base64?: string;
    blob?: Blob | any;
};

export interface i_folder_file {
    name: string;
    thumbnail: boolean;
    type: string;
};

export interface i_format_date {
    date_ISO: string;
    day: string;
    full_date: string;
    hour: string;
    minutes: string;
    month: string;
    obj_date: string | Date;
    seconds: string;
    year: string;
};

export interface i_hsl {
    array: any[];
    string: string;
};

export interface i_identification {
    type_document: string;
    number_document: string;
};

export interface i_information_devices {
    device?: string;
    desktop: boolean;
    mobile: boolean;
    orientation?: string;
    os: string;
    os_version?: string;
    tablet: boolean;
};

export interface i_items {
    index?: any;
    placeholder: string;
    selected?: boolean;
    show_hide?: boolean;
    value: any;
};

export interface i_language {
    abbreviation: string;
    name: string;
};

export interface i_links {
    href: string;
    id?: string;
    placeholder: string;
    selected?: boolean;
    show_hide?: boolean;
    target?: string;
};

export interface i_maps {
    googleMaps: string;
    openStreetMaps: string;
};

export interface i_person {
    email?: string;
    id?: any;
    letter?: string;
    lookup?: any;
    name: string;
    number_phone?: string;
    identification?: i_identification;
};

export interface i_phone {
    dial_code: string;
    number_phone: string;
};

export interface i_rgb {
    array: any[];
    string: string;
};

export interface i_size_devices {
    height: number;
    width: number;
};