import { HeaderLyComponent } from "./layouts/header-ly/header-ly.component";

import { ButtonComponent } from "./ui_ux/button/button.component";
import { InputSearchComponent } from "./ui_ux/input-search/input-search.component";
import { LinkComponent } from "./ui_ux/link/link.component";
import { PagerComponent } from "./ui_ux/pager/pager.component";

import { CommunicationComponent } from "./for_modules/communication/communication.component";
import { PageCommunicationComponent } from "./for_modules/page-communication/page-communication.component";

import { CommunicationLoadComponent } from "./loaders/for_modules/communication-load/communication-load.component";

export const components: any[] = [
    HeaderLyComponent,

    ButtonComponent,
    InputSearchComponent,
    LinkComponent,
    PagerComponent,

    CommunicationComponent,
    PageCommunicationComponent,

    CommunicationLoadComponent,
];

export * from "./layouts/header-ly/header-ly.component";

export * from "./ui_ux/button/button.component";
export * from "./ui_ux/input-search/input-search.component";
export * from "./ui_ux/link/link.component";
export * from "./ui_ux/pager/pager.component";

export * from "./for_modules/communication/communication.component";

export * from "./loaders/for_modules/communication-load/communication-load.component";