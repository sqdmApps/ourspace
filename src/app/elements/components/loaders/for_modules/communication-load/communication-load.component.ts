import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-communication-load',
  templateUrl: './communication-load.component.html',
  styleUrls: ['./communication-load.component.css']
})
export class CommunicationLoadComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  number_date: {
    width: string;
  }[] = [];
  number_titles: {
    width: string;
  }[] = [];
  date_line_number: number = 1;
  title_line_number: number = 1;
  width_random: any;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) {
    const width_random = Math.floor(Math.random() * (100 - 60)) + 60;
    this.width_random = {
      "width": `${width_random}%`,
    };
    this.date_line_number = Math.floor(Math.random() * 2);
    this.title_line_number = Math.floor(Math.random() * 2);
  };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `c_communication_load__${this.id}`;
      this.on_resize(null);
    }, 400);
    this.date_line_number = this.date_line_number == 0 ? 1 : this.date_line_number;
    this.title_line_number = this.title_line_number == 0 ? 1 : this.title_line_number;
    for (let a = 0; a < this.date_line_number; a++) {
      const width = Math.floor(Math.random() * (100 - 60)) + 60;
      this.number_date.push({
        width: `${width}%`,
      });
    };
    for (let a = 0; a < this.title_line_number; a++) {
      const width = Math.floor(Math.random() * (100 - 60)) + 60;
      this.number_titles.push({
        width: `${width}%`,
      });
    };
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    this.component.classList.add(devices.device.os.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
