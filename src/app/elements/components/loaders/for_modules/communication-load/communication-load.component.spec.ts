import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommunicationLoadComponent } from './communication-load.component';

describe('CommunicationLoadComponent', () => {
  let component: CommunicationLoadComponent;
  let fixture: ComponentFixture<CommunicationLoadComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommunicationLoadComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CommunicationLoadComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
