import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { color } from 'html2canvas/dist/types/css/types/color';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-button',
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.css']
})
export class ButtonComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  @Input() active?: boolean;
  @Input() alignment: string = "left";
  @Input() icon_code?: string;
  @Input() icon_code_on?: string;
  @Input() name?: string;
  @Input() name_color: string = "var(--black)";
  @Input() placeholder: string = "ui_ux.t0000";
  @Input() placeholder_on?: string;
  @Input() radio_borders: string = "var(--spacing_s)";
  @Input() theme?: string;
  @Input() tooltip?: string;

  @Output() clicked: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  color_for_name = {
    color: "",
  };
  styles_content = {
    "border-radius": "",
  };
  type: string = "default";

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `button_qix_${this.id}`;
      this.on_resize(null);
    }, 400);
    this.alignment = this.alignment ? this.alignment.toLowerCase() : "left";
    this.theme = this.theme ? this.theme.toLowerCase() : "default";
    this.type = (this.icon_code) ? "with_icon" : this.type;
    this.type = (this.icon_code && !this.placeholder) ? "only_icon" : this.type;
    this.type = (this.icon_code && !this.placeholder && this.name) ? "with_name" : this.type;
    this.color_for_name.color = this.name_color;
    this.icon_code = this.active && this.icon_code_on ? this.icon_code_on : this.icon_code;
    this.styles_content['border-radius'] = this.radio_borders;
  };

  on_click(): void {
    setTimeout(() => {
      let click: i_events_action = {
        element: this.component,
        event: "click",
        value: true,
      };
      this.clicked.emit(click);
    }, 400);
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    let browser: string = this.s_common.get_browser() ?? "";
    this.component.classList.add(devices.device.os.toLowerCase());
    this.component.classList.add(browser.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
