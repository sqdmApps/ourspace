import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-link',
  templateUrl: './link.component.html',
  styleUrls: ['./link.component.css']
})
export class LinkComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  @Input() href?: string;
  @Input() icon_code?: string;
  @Input() placeholder: string = "ui_ux.t0001";
  @Input() theme?: string;
  @Input() tooltip?: string;

  @Output() clicked: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  type: string = "default";

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `link_qix_${this.id}`;
      this.on_resize(null);
    }, 400);
    this.theme = this.theme ? this.theme.toLowerCase() : "default";
    this.type = (this.icon_code) ? "with_icon" : this.type;
    this.type = (!this.icon_code && !this.placeholder) ? "only_icon" : this.type;
  };

  on_click(): void {
    setTimeout(() => {
      let click: i_events_action = {
        element: this.component,
        event: "click",
      };
      if (this.href) {
        if (this.href.indexOf("http") >= 0) {
          this.s_common.go_to_link(this.href, "blank");
        } else {
          this.s_common.go_to(this.href);
        };
        click.value = true;
      } else {
        click.value = false;
      };
      this.clicked.emit(click);
    }, 400);
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    this.component.classList.add(devices.device.os.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
