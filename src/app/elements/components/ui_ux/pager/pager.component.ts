import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-pager',
  templateUrl: './pager.component.html',
  styleUrls: ['./pager.component.css']
})
export class PagerComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  @Input() type?: string = "left";
  @Input() theme?: string;

  @Output() next: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  @Output() previous: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `pager_qix_${this.id}`;
      this.on_resize(null);
    }, 400);
    this.type = this.type ? this.type.toLowerCase() : "left";
    this.theme = this.theme ? this.theme.toLowerCase() : this.theme;
  };

  on_next(): void {
    let click: i_events_action = {
      element: this.component,
      event: "next",
      value: true,
    };
    this.next.emit(click);
  };

  on_previous(): void {
    let click: i_events_action = {
      element: this.component,
      event: "previous",
      value: true,
    };
    this.previous.emit(click);
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    this.component.classList.add(devices.device.os.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
