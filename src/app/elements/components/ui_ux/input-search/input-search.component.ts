import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices, i_items } from 'src/app/elements/interfaces/common/common';
import { i_process } from 'src/app/elements/interfaces/for_modules/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-input-search',
  templateUrl: './input-search.component.html',
  styleUrls: ['./input-search.component.css']
})
export class InputSearchComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: any;
  html_elements!: any;
  phone_mode: boolean = true;

  @Input() data_list: i_items[] = [];
  @Input() list_to_search: string = "default";
  @Input() name!: string;
  @Input() placeholder: string = "ui_ux.t0005";
  @Input() required: boolean = false;

  active_mode: boolean = false;
  error: boolean = false;
  error_message: string = "feedback.ui_ux.e00";
  focus_mode: boolean = false;
  search_list!: i_items[];

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `input_search_qix_${this.id}`;
      this.component.input_search = document.getElementById(`inp_search_qix_${this.id}`) ?? null;
      this.on_resize(null);
    }, 400);
    this.placeholder = this.placeholder ? this.s_translate.instant(this.placeholder) : this.placeholder;
    let search_list: i_items[] = Array<i_items>();
    switch (this.list_to_search) {
      case "default":
        search_list = this.s_common.get_data_json("placeholder_list_search") ?? [];
        this.search_list = search_list;
        break;
      case "process":
        let all_process: i_process[] = this.s_common.get_all_process() ?? [];
        for (let a = 0; a < all_process.length; a++) {
          const process: i_process = all_process[a];
          let item: i_items = {
            index: process.id,
            placeholder: process.name ?? "",
            selected: false,
            show_hide: true,
            value: process.path ?? "",
          };
          search_list.push(item);
        };
        this.search_list = search_list;
        break;
      default:
        search_list = this.data_list.length > 0 ? this.data_list : Array<i_items>();
        this.search_list = search_list;
        break;
    };
  };

  on_blur(data: Event): void {
    this.component.input_search = data.target;
    this.focus_mode = false;
    this.active_mode = this.component.input_search.value ? true : false;
  };

  on_focus(data: Event): void {
    this.component.input_search = data.target;
    this.focus_mode = true;
    this.active_mode = this.component.input_search.value ? true : false;
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    this.component.classList.add(devices.device.os.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
