import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PageCommunicationComponent } from './page-communication.component';

describe('PageCommunicationComponent', () => {
  let component: PageCommunicationComponent;
  let fixture: ComponentFixture<PageCommunicationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PageCommunicationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(PageCommunicationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
