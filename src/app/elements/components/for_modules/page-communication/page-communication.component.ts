import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { i_communication } from 'src/app/elements/interfaces/for_modules/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-page-communication',
  templateUrl: './page-communication.component.html',
  styleUrls: ['./page-communication.component.css']
})
export class PageCommunicationComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  @Input() data: i_communication = {
    creation_date: {
      value: new Date(),
      placeholder: this.s_common.serialize_date(new Date()),
    },
    id: this.s_common.guid_generator(),
    mail_piece: {
      base64: "./../../../../../assets/images/example.svg",
      blob: null,
    },
    title: "components.t0000",
  };

  full_view_mode: boolean = false;
  with_external_links?: boolean;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) { };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `c_page_communication__${this.id}`;
      this.on_resize(null);
    }, 400);
    let length_external_link: any = this.data.external_links?.length;
    this.with_external_links = length_external_link > 0 ? true : false;
  };

  off_full_view_mode(): void {
    this.full_view_mode = false;
  };

  on_full_view_mode(): void {
    if (this.phone_mode) {
      setTimeout(() => {
        this.full_view_mode = true;
      }, 400);
    };
  };

  on_resize(data: any): void {
    let devices: i_devices = this.s_common.get_device();
    this.component.classList.add(devices.device.os.toLowerCase());
    this.phone_mode = devices.size.width >= 720 ? false : true;
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };

}
