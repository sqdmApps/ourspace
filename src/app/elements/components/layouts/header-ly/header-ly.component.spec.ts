import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HeaderLyComponent } from './header-ly.component';

describe('HeaderLyComponent', () => {
  let component: HeaderLyComponent;
  let fixture: ComponentFixture<HeaderLyComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HeaderLyComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HeaderLyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
