import { TestBed } from '@angular/core/testing';

import { LoginLgService } from './login-lg.service';

describe('LoginLgService', () => {
  let service: LoginLgService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(LoginLgService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
