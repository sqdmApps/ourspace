import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { o_auth_settings } from "src/oauth";
import { EncryptStorage } from 'storage-encryption';
import { CommonService } from '../../common/common.service';

@Injectable({
  providedIn: 'root'
})
export class LoginLgService {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  private token!: string;
  public authenticated: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_http: HttpClient,
    private s_msal: MsalService,
  ) { }

  async get_access_token(): Promise<string> {
    const account = this.s_msal.instance.getAllAccounts()[0];
    const result = await this.s_msal
      .acquireTokenSilent({
        account: account ?? undefined,
        scopes: o_auth_settings.scopes,
      })
      .toPromise()
      .catch((reason: any) => {
        console.log(JSON.stringify(reason, null, 2));
      });
    if (result) {
      this.token = result.accessToken;
      return result.accessToken;
    }
    this.authenticated = false;
    return "";
  };

  async sign_in(): Promise<void> {
    const result = await this.s_msal
      .loginPopup(o_auth_settings)
      .toPromise()
      .catch((reason) => {
        console.log(JSON.stringify(reason));
      });
    if (result) {
      this.s_msal.instance.setActiveAccount(result.account);
      this.authenticated = true;
      this.token = result.accessToken;
      this.session_data.encrypt("login", "true");
    } else {
      this.authenticated = false;
      this.token = "";
      this.session_data.encrypt("login", "false");
    };
  };

  async log_out(): Promise<void> {
    this.authenticated = false;
    this.session_data.remove("login");
    await this.s_msal.logout().toPromise().then((response) => {
      this.s_common.go_to("login");
    });
  }
}
