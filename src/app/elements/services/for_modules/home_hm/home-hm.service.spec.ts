import { TestBed } from '@angular/core/testing';

import { HomeHmService } from './home-hm.service';

describe('HomeHmService', () => {
  let service: HomeHmService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(HomeHmService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
