import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { MsalService } from '@azure/msal-angular';
import { o_auth_settings } from 'src/oauth';
import { EncryptStorage } from 'storage-encryption';
import { CommonService } from '../../common/common.service';
import { environment } from 'src/environments/environment';
import { i_communication } from 'src/app/elements/interfaces/for_modules/for_modules';
import { i_links } from 'src/app/elements/interfaces/common/common';

@Injectable({
  providedIn: 'root'
})
export class HomeHmService {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  private token!: string;
  public authenticated: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_http: HttpClient,
    private s_msal: MsalService,
  ) { }

  format_communication(array: any): any {
    let format = Array<i_communication>();
    let items: any[] = [];
    for (let index = 0; index < array.length; index++) {
      const fields = array[index].fields;
      fields.createdBy = array[index].createdBy.user;
      fields.modifiedBy = array[index].lastModifiedBy.user;
      items.push(fields);
    }
    for (let a = 0; a < items.length; a++) {
      const item_response: any = items[a];
      let item: i_communication = {
        creation_date: {
          placeholder: item_response.Created ? this.s_common.serialize_date(item_response.Created) : this.s_common.serialize_date(new Date()),
          value: item_response.Created ? new Date(item_response.Created) : new Date(),
        },
        id: item_response.id ? item_response.id : "",
        mail_piece: {
          base64: item_response.mail_piece ? item_response.mail_piece.replace("unsafe:", "") : "",
        },
        modification_date: {
          placeholder: item_response.Modified ? this.s_common.serialize_date(item_response.Modified) : this.s_common.serialize_date(new Date()),
          value: item_response.Modified ? new Date(item_response.Modified) : new Date(),
        },
        title: item_response.Title ? item_response.Title : "components.t0000",
      };
      let links_button: string[] = item_response.links_buttons ? item_response.links_buttons.split(";") : [];
      let texts_buttons: string[] = item_response.texts_buttons ? item_response.texts_buttons.split(";") : [];
      let external_links: i_links[] = [];
      for (let b = 0; b < links_button.length; b++) {
        const item_link = links_button[b];
        let link: i_links = {
          href: item_link,
          placeholder: texts_buttons[b],
        };
        external_links.push(link);
      };
      item.external_links = external_links;
      format.push(item);
    }
    return format;
  };

  async get_access_token(): Promise<string> {
    const account = this.s_msal.instance.getAllAccounts()[0];
    const result = await this.s_msal
      .acquireTokenSilent({
        account: account ?? undefined,
        scopes: o_auth_settings.scopes,
      })
      .toPromise()
      .catch((reason: any) => {
        console.log(JSON.stringify(reason, null, 2));
      });
    if (result) {
      this.token = result.accessToken;
      return result.accessToken;
    }
    this.authenticated = false;
    return "";
  };

  async read(api_key: any, id?: string, options?: string): Promise<any> {
    let all_items: any;
    id = id ? id : "";
    options = options ? options : "";
    let url = environment.api_database + api_key + id + options;
    const token = await this.get_access_token().catch((reason) => { });
    if (token) {
      const headers = new HttpHeaders().append("Authorization", `Bearer ${token}`);
      try {
        all_items = await this.s_http.get<any>(url, { headers: headers }).toPromise();
        return all_items;
      } catch (error: any) {
        //alert(error);
        let result: any;
        return all_items;
      };
    } else {
      return all_items;
    };
  }

  async read_communications(id?: string): Promise<any> {
    let api_key = environment.api_key_communications;
    let response: any;
    let options = "/items?expand=fields";
    response = await this.read(api_key, id, options).catch((result) => { })
    switch (api_key) {
      case environment.api_key_communications:
        response = response ? this.format_communication(response.value) : response;
        break;
      default:
        break;
    };
    return response
  };
}
