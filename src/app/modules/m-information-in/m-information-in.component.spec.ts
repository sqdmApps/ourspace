import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MInformationInComponent } from './m-information-in.component';

describe('MInformationInComponent', () => {
  let component: MInformationInComponent;
  let fixture: ComponentFixture<MInformationInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MInformationInComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MInformationInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
