import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MInformationInRoutingModule } from './m-information-in-routing.module';
import { MInformationInComponent } from './m-information-in.component';
import { NoFoundInComponent } from './pages/no-found-in/no-found-in.component';
import { TranslateModule } from '@ngx-translate/core';
import { ElementsModule } from 'src/app/elements/elements.module';
import { MaintenanceInComponent } from './pages/maintenance-in/maintenance-in.component';


@NgModule({
  declarations: [
    MInformationInComponent,
    NoFoundInComponent,
    MaintenanceInComponent
  ],
  imports: [
    CommonModule,
    MInformationInRoutingModule,
    TranslateModule,
    ElementsModule,
  ]
})
export class MInformationInModule { }
