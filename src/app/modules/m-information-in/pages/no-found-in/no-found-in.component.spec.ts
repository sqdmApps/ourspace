import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NoFoundInComponent } from './no-found-in.component';

describe('NoFoundInComponent', () => {
  let component: NoFoundInComponent;
  let fixture: ComponentFixture<NoFoundInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NoFoundInComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NoFoundInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
