import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_devices, i_events_action } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-no-found-in',
  templateUrl: './no-found-in.component.html',
  styleUrls: ['./no-found-in.component.css']
})
export class NoFoundInComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_translate: TranslateService,
  ) {
    this.s_common.change_name_tab_translation("information.no_found.t0003");
  };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `no_found_in`;
      this.on_resize(null);
    }, 400);
  };

  on_back(): void {
    this.s_common.go_to("back");
  };

  on_resize(data: any): void {
    let device: i_devices = this.s_common.get_device() ?? <i_devices>{};
    let no_found_in: HTMLElement = this.s_element_ref.nativeElement.childNodes[0];
    this.phone_mode = device.size.width >= 720 ? false : true;
    if (device.size.width >= 720) {
    } else {
      no_found_in ? no_found_in.style.height = "calc(100vh - 120px)" : null;
    };
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
