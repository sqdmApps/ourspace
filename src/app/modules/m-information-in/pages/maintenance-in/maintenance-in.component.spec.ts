import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MaintenanceInComponent } from './maintenance-in.component';

describe('MaintenanceInComponent', () => {
  let component: MaintenanceInComponent;
  let fixture: ComponentFixture<MaintenanceInComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MaintenanceInComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MaintenanceInComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
