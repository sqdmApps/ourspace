import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MInformationInComponent } from './m-information-in.component';
import { NoFoundInComponent } from './pages/no-found-in/no-found-in.component';
import { MaintenanceInComponent } from './pages/maintenance-in/maintenance-in.component';

const routes: Routes = [
  {
    path: '',
    component: MInformationInComponent,
    children: [
      {
        path: 'no_found',
        component: NoFoundInComponent,
      },
      {
        path: 'maintenance',
        component: MaintenanceInComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MInformationInRoutingModule { }
