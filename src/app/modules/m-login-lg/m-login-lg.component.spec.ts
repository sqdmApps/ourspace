import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MLoginLgComponent } from './m-login-lg.component';

describe('MLoginLgComponent', () => {
  let component: MLoginLgComponent;
  let fixture: ComponentFixture<MLoginLgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MLoginLgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MLoginLgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
