import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MLoginLgComponent } from './m-login-lg.component';
import { LoginLgComponent } from './pages/login-lg/login-lg.component';

const routes: Routes = [{
  path: '',
  component: MLoginLgComponent,
  children: [
    {
      path: 'login',
      component: LoginLgComponent,
    }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MLoginLgRoutingModule { }
