import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MLoginLgRoutingModule } from './m-login-lg-routing.module';
import { MLoginLgComponent } from './m-login-lg.component';
import { TranslateModule } from '@ngx-translate/core';
import { ElementsModule } from 'src/app/elements/elements.module';
import { LoginLgComponent } from './pages/login-lg/login-lg.component';


@NgModule({
  declarations: [
    MLoginLgComponent,
    LoginLgComponent
  ],
  imports: [
    CommonModule,
    MLoginLgRoutingModule,
    TranslateModule,
    ElementsModule,
  ]
})
export class MLoginLgModule { }
