import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices } from 'src/app/elements/interfaces/common/common';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { LoginLgService } from 'src/app/elements/services/for_modules/login_lg/login-lg.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-login-lg',
  templateUrl: './login-lg.component.html',
  styleUrls: ['./login-lg.component.css']
})
export class LoginLgComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_login: LoginLgService,
    private s_translate: TranslateService,
  ) {
    sessionStorage.clear();
    this.s_common.change_name_tab_translation("login.login.t0000");
  };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `login_lg`;
      this.on_resize(null);
    }, 400);
  };

  async on_login(): Promise<void> {
    await this.s_login.sign_in().then((promise: any) => {
      if (this.s_login.authenticated) {
        let visited = this.session_data.decrypt("url_visited_fail") ? this.session_data.decrypt("url_visited_fail") : null;
        if (visited) {
          this.session_data.remove("url_visited_fail");
          this.s_common.go_to(visited);
        } else {
          this.s_common.go_to("feed");
        };
      } else {
        alert(this.s_translate.instant("feedback.login.e00"));
      };
    }).catch((error: any) => {
      alert(error);
    });
  };

  on_resize(data: any): void {
    let device: i_devices = this.s_common.get_device() ?? <i_devices>{};
    let login_lg: HTMLElement = this.s_element_ref.nativeElement.childNodes[0];
    this.phone_mode = device.size.width >= 720 ? false : true;
    if (device.size.width >= 720) {
    } else {
      login_lg ? login_lg.style.height = "calc(100vh - 120px)" : null;
    };
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
