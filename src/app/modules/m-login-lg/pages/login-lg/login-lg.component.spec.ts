import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginLgComponent } from './login-lg.component';

describe('LoginLgComponent', () => {
  let component: LoginLgComponent;
  let fixture: ComponentFixture<LoginLgComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LoginLgComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginLgComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
