import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FeedHmComponent } from './feed-hm.component';

describe('FeedHmComponent', () => {
  let component: FeedHmComponent;
  let fixture: ComponentFixture<FeedHmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FeedHmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FeedHmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
