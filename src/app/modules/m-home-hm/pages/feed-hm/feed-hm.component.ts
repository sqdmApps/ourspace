import { Component, ElementRef, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { i_events_action, i_devices, i_columns, i_links } from 'src/app/elements/interfaces/common/common';
import { i_communication, i_process } from 'src/app/elements/interfaces/for_modules/for_modules';
import { CommonService } from 'src/app/elements/services/common/common.service';
import { HomeHmService } from 'src/app/elements/services/for_modules/home_hm/home-hm.service';
import { LoginLgService } from 'src/app/elements/services/for_modules/login_lg/login-lg.service';
import { EncryptStorage } from 'storage-encryption';

@Component({
  selector: 'app-feed-hm',
  templateUrl: './feed-hm.component.html',
  styleUrls: ['./feed-hm.component.css']
})
export class FeedHmComponent implements OnInit {
  public session_data = new EncryptStorage("SECRET_KEY", "sessionStorage");
  public local_data = new EncryptStorage("SECRET_KEY", "localStorage");

  @Input() id?: string;
  @Input() class?: string;
  @Output() init: EventEmitter<i_events_action> = new EventEmitter<i_events_action>();
  component!: HTMLElement | any;
  html_elements!: any;
  phone_mode: boolean = true;

  communication_to_open!: i_communication;
  current_space: string = "home.feed.t0000";
  internal_window_mode: boolean = false;
  link_button_list!: i_links[];
  list_communications!: i_communication[];
  list_communicated_left!: i_communication[];
  list_communicated_right!: i_communication[];
  loader_mode: boolean = true;
  open_spaces: boolean = false;
  process_to_open!: string;
  request_button_list!: i_links[];
  ticket_button_list!: i_links[];
  with_pager_communications: boolean = false;

  constructor(
    private s_common: CommonService,
    private s_element_ref: ElementRef,
    private s_home_hm: HomeHmService,
    private s_login_lg: LoginLgService,
    private s_translate: TranslateService,
  ) {
    this.s_common.change_name_tab_translation("home.feed.t0000");
    this.s_common.validate_login();

    this.link_button_list = this.s_common.get_data_json("links");
    this.request_button_list = this.s_common.get_data_json("requests");
    this.ticket_button_list = this.s_common.get_data_json("tickets");
    this.get_communications();
    this.get_all_process();
  };

  async get_communications(): Promise<void> {
    let list_communications: i_communication[] = await this.s_home_hm.read_communications();
    list_communications = this.s_common.sort_decent_date_obj(list_communications, "creation_date");
    this.list_communications = list_communications;
    this.loader_mode = this.list_communications ? this.list_communications.length > 0 ? false : true : true;
    let columns_communications: i_columns = this.list_communications ? this.s_common.separate_array_2_columns(this.list_communications) : <i_columns>{};
    this.list_communicated_left = columns_communications.columns ? columns_communications.columns.left : [];
    this.list_communicated_right = columns_communications.columns ? columns_communications.columns.right : [];
    this.with_pager_communications = columns_communications.count > 16 ? true : false;
  };

  get_all_process(): void {
    let all_process: i_process[] = this.s_common.get_all_process() ?? [];
  };

  ngOnInit(): void {
    setTimeout(() => {
      this.component = this.s_element_ref.nativeElement.childNodes[0];
      this.component.name = `feed_hm`;
      this.on_resize(null);
    }, 400);
  };

  on_change_space(space_name: string, path: string): void {
    this.current_space = space_name;
    this.s_common.go_to(path);
  };

  on_close_session(): void {
    let close_session: boolean = confirm(this.s_translate.instant("feedback.login.c00") ?? "Are you sure you want to log out?");
    if (close_session) {
      this.s_login_lg.log_out();
    };
  };

  on_communication(communication: i_communication): void {
    this.process_to_open = "communication";
    this.internal_window_mode = true;
    this.communication_to_open = communication;
  };

  on_list_options(data: i_links): void {
    this.process_to_open = data.id ?? "";
    this.internal_window_mode = data.id && data.target == "iframe" ? true : false;
  };

  off_list_options(): void {
    this.process_to_open = "";
    this.internal_window_mode = false;
  };

  on_off_space(): void {
    setTimeout(() => {
      this.open_spaces = !this.open_spaces ? true : false;
    }, 400);
  };

  on_resize(data: any): void {
    let device: i_devices = this.s_common.get_device() ?? <i_devices>{};
    let feed_hm: HTMLElement = this.s_element_ref.nativeElement.childNodes[0];
    this.phone_mode = device.size.width >= 720 ? false : true;
    if (device.size.width >= 720) {
    } else {
      feed_hm ? feed_hm.style.height = "calc(100vh - 120px)" : null;
    };
    let init: i_events_action = {
      element: this.component,
      event: "load",
      value: this.component,
    };
    this.init.emit(init);
  };
}
