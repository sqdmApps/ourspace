import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { MHomeHmRoutingModule } from './m-home-hm-routing.module';
import { MHomeHmComponent } from './m-home-hm.component';
import { TranslateModule } from '@ngx-translate/core';
import { ElementsModule } from 'src/app/elements/elements.module';
import { FeedHmComponent } from './pages/feed-hm/feed-hm.component';


@NgModule({
  declarations: [
    MHomeHmComponent,
    FeedHmComponent,
  ],
  imports: [
    CommonModule,
    MHomeHmRoutingModule,
    TranslateModule,
    ElementsModule,
  ]
})
export class MHomeHmModule { }
