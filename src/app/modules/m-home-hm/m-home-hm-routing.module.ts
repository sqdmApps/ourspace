import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MHomeHmComponent } from './m-home-hm.component';
import { FeedHmComponent } from './pages/feed-hm/feed-hm.component';

const routes: Routes = [
  {
    path: '',
    component: MHomeHmComponent,
    children: [
      {
        path: 'feed',
        component: FeedHmComponent,
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MHomeHmRoutingModule { }
