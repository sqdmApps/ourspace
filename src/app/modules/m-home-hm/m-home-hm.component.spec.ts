import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MHomeHmComponent } from './m-home-hm.component';

describe('MHomeHmComponent', () => {
  let component: MHomeHmComponent;
  let fixture: ComponentFixture<MHomeHmComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MHomeHmComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MHomeHmComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
