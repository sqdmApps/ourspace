import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: "", redirectTo: "login", pathMatch: "full",
  },
  {
    path: '',
    loadChildren: () => import('./modules/m-information-in/m-information-in.module').then(m => m.MInformationInModule),
  },
  {
    path: '',
    loadChildren: () => import('./modules/m-login-lg/m-login-lg.module').then(m => m.MLoginLgModule)
  },
  {
    path: '',
    loadChildren: () => import('./modules/m-home-hm/m-home-hm.module').then(m => m.MHomeHmModule)
  },
  {
    path: "**", redirectTo: "maintenance", pathMatch: "full",
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
